Example Module
==============

![screenshot](https://gitlab.com/francoisjacquet/Example/raw/master/screenshot.png?inline=false)

http://gitlab.com/francoisjacquet/Example

Version 2.3 - October, 2021

License GNU GPL v2

Author François Jacquet

DESCRIPTION
-----------
This module serves as an example for developers who wish to create their own module for RosarioSIS.
You will find useful comments in each file.
Inline help.
Consult the [Starter Guide](https://gitlab.com/francoisjacquet/Example/wikis/Starter-Guide)!

CONTENT
-------
Example
- Example Widget
- Setup

Resources
- Example Resource

INSTALL
-------
Copy the `Example/` folder (if named `Example-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 6.8+
