<?php
/**
 * English Help texts
 *
 * Texts are organized by:
 * - Module
 * - Profile
 *
 * Please use this file as a model to translate the texts to your language
 * The new resulting Help file should be named after the following convention:
 * Help_[two letters language code].php
 *
 * @author François Jacquet
 *
 * @package Example module
 * @subpackage Help
 */

// EXAMPLE ---.
if ( User( 'PROFILE' ) === 'admin' ) :

	$help['Example/ExampleResource.php'] = '<p>' . _help( '<i>Example Resource</i> lets you consult an example <b>resource</b>.', 'Example' ) . '</p>
	<p>' . _help( 'I am the inline help for the <code>ExampleResource.php</code> program, you will find me in the <code>Help_en.php</code> file, see you!', 'Example' ) . '</p>';

	$help['Example/ExampleWidget.php'] = '<p>' . _help( '<i>Example Widget</i> lets you use an example <b>widget</b>.', 'Example' ) . '</p>
	<p>' . _help( 'I am the inline help for the <code>ExampleWidget.php</code> program, you will find me in the <code>Help_en.php</code> file, see you!', 'Example' ) . '</p>';

	$help['Example/Setup.php'] = '<p>' . _help( '<i>Setup</i> lets you <b>configure</b> the module.', 'Example' ) . '</p>
	<p>' . _help( 'I am the inline help for the <code>Setup.php</code> program, you will find me in the <code>Help_en.php</code> file, see you!', 'Example' ) . '</p>';

endif;


// Teacher help.
if ( User( 'PROFILE' ) === 'teacher' ) :

	$help['Example/ExampleWidget.php'] = '<p>' . _help( '<i>Example Widget</i> lets you use an example <b>widget</b>.', 'Example' ) . '</p>
	<p>' . _help( 'I am the inline help for the <code>ExampleWidget.php</code> program, you will find me in the <code>Help_en.php</code> file, see you!', 'Example' ) . '</p>
	<p>' . _help( 'This text should be readable by <b>teachers</b> only!', 'Example' ) . '</p>';

endif;


// Parent & student help.
if ( User( 'PROFILE' ) === 'parent' ) :

	$help['Example/ExampleWidget.php'] = '<p>' . _help( '<i>Example Widget</i> lets you use an example <b>widget</b>.', 'Example' ) . '</p>
	<p>' . _help( 'I am the inline help for the <code>ExampleWidget.php</code> program, you will find me in the <code>Help_en.php</code> file, see you!', 'Example' ) . '</p>
	<p>' . _help( 'This text should be readable by <b>parents or students</b> only!', 'Example' ) . '</p>';

endif;
