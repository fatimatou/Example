<?php
/**
 * Setup program
 * Optional
 * - Modify the Config values present in the program_config table
 *
 * @package Example module
 */

DrawHeader( ProgramTitle() ); // Display main header with Module icon and Program title.

if ( $_REQUEST['modfunc'] === 'update'
	&& AllowEdit() ) // AllowEdit must be verified before inserting, updating, deleting data.
{
	if ( isset( $_POST['values'] ) )
	{
		// Verify value is numeric.
		if ( empty( $_REQUEST['values']['EXAMPLE_CONFIG'] )
			|| is_numeric( $_REQUEST['values']['EXAMPLE_CONFIG'] ) )
		{
			ProgramConfig( 'example', 'EXAMPLE_CONFIG', $_REQUEST['values']['EXAMPLE_CONFIG'] );

			// Add note.
			$note[] = button( 'check' ) . '&nbsp;' .
				dgettext( 'Example', 'The configuration value has been modified.' );
		}
		else // If no value or value not numeric.
		{
			// Add error message.
			$error[] = _( 'Please enter valid Numeric data.' );
		}
	}

	// Unset modfunc & values & redirect URL.
	RedirectURL( [ 'modfunc', 'values' ] );
}

// Display Setup value form.
if ( empty( $_REQUEST['modfunc'] ) )
{
	// Display note if any.
	echo ErrorMessage( $note, 'note' );

	// Display errors if any.
	echo ErrorMessage( $error, 'error' );

	// Form used to send the updated Config to be processed by the same script (see at the top).
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] .
		'&modfunc=update' ) . '" method="POST">';

	// Display secondary header with Save button (aligned right).
	DrawHeader( '', SubmitButton() ); // SubmitButton is diplayed only if AllowEdit.

	echo '<br />';

	// Encapsulate content in PopTable.
	PopTable( 'header', dgettext( 'Example', 'Example module Setup' ) );

	// Display the program config options.
	echo '<fieldset><legend><b>' . dgettext( 'Example', 'Example' ) . '</b></legend><table>';

	echo '<tr style="text-align:left;"><td>' .
		TextInput(
			ProgramConfig( 'example', 'EXAMPLE_CONFIG' ),
			'values[EXAMPLE_CONFIG]',
			'<span class="legend-gray" title="' . dgettext( 'Example', 'Try to enter a non-numeric value' ) . '">' .
				dgettext( 'Example', 'Example config value label' ) . ' *</span>',
			'maxlength=2 size=2 min=0'
		) . '</td></tr>';

	echo '</table></fieldset>';

	// Close PopTable.
	PopTable( 'footer' );

	// SubmitButton is diplayed only if AllowEdit.
	echo '<br /><div class="center">' . SubmitButton() . '</div>';

	echo '</form>';
}
